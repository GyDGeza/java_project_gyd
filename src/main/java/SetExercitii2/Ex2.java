package SetExercitii2;

import java.util.Scanner;

public class Ex2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Citeste lungime array: ");
        int length = sc.nextInt();
        String[] sline = stringer(length);
        System.out.println("Aranjeaza: \nCrescator - <\nDescrescator - >");
        sc.nextLine();
        String sel = sc.nextLine();
        if (sel.equals(">")) {
            sline = stringArrangeHighLow(sline);
            for (int i = 0; i < sline.length; i++) {
                System.out.println(sline[i]);
            }
        }
        else if (sel.equals("<"))
        {
            sline = stringArrangeLowHigh(sline);
            for (int i = 0; i < sline.length; i++) {
                System.out.println(sline[i]);
            }
        }
        else
        {
            System.out.println("optiune invalida");
        }
    }

    public static String[] stringer(int x)
    {
        Scanner sc = new Scanner(System.in);
        String[] sline = new String[x];
        for (int i = 0; i<x; i++)
        {
            System.out.print("Citeste stringul: ");
            sline[i] = sc.nextLine();
        }
        return sline;
    }

    public static String[] stringArrangeHighLow(String[] x)
    {
        String aux;
        for (int i=0; i<x.length; i++)
        {
            for (int j=i+1; j<x.length; j++)
            {
                if (x[i].length() < x[j].length())
                {
                    aux = x[i];
                    x[i] = x[j];
                    x[j] = aux;
                }
            }
        }
        return x;
    }

    public static String[] stringArrangeLowHigh(String[] x)
    {
        String aux;
        for (int i=0; i<x.length; i++)
        {
            for (int j=i+1; j<x.length; j++)
            {
                if (x[i].length() > x[j].length())
                {
                    aux = x[i];
                    x[i] = x[j];
                    x[j] = aux;
                }
            }
        }
        return x;
    }
}
