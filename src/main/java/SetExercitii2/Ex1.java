package SetExercitii2;

import java.util.Scanner;

public class Ex1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Citeste stringul: ");
        String sline = sc.nextLine();
        sline = zeroArrange(sline);
        System.out.println(sline);
    }

    public static String zeroArrange(String x)
    {
        int cnt = 0;
        for (int i = 0; i<x.length(); i++)
        {
            if (x.charAt(i) == '0')
            {
                cnt++;
            }
        }
        x = x.replace("0", "");
        for (int i = 0; i<cnt; i++)
        {
            x = x.concat("0");
        }
        return x;
    }
}
