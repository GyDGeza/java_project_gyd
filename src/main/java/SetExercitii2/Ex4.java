package SetExercitii2;

import java.util.Scanner;

public class Ex4 {
    public static void main(String[] args) {

            Scanner sc = new Scanner(System.in);
            System.out.print("Citeste numar: ");
            int num = sc.nextInt();
            int num2 = reverser(num);
            if (num == num2){
            System.out.println("Numarul este palindrom");}
            else
            {
                System.out.println("Numarul nu este palindrom");
            }
        }

        public static int reverser(int x)
        {
            int aux = 0;
            while (x !=0)
            {
                aux = aux*10 + x%10;
                x = x/10;
            }
            return aux;
        }

}
