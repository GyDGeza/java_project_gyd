package SetExercitii2;

import java.util.Scanner;

public class Ex5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Citeste numar: ");
        int num = sc.nextInt();
        int[] fib = new int[num];
        for (int i=0; i<num; i++)
        {
            if (i==0)
            {
                fib[i] = 0;
            }
            else if (i==1)
            {
                fib[i] = 1;
            }
            else {
                fib[i] = fib[i-1] + fib[i-2];
            }
            System.out.print(fib[i] + " ");
        }
    }
}
