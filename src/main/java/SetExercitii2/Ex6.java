package SetExercitii2;

import java.util.Scanner;

public class Ex6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Citeste stringul: ");
        String sline = sc.nextLine();
        System.out.print("Citeste echivalent pentru 'a': ");
        String shift = sc.nextLine();
        int shifter = 0;
        if (shift.charAt(0) <= 'z') {
            shifter = shift.charAt(0) - 97;
        }
        else {
            shifter = shift.charAt(0) - 65;
        }
        System.out.print("Alfabet: a b c d e f g h i j k l m n o p q r s t u v w x y z\nCheie:   ");
        for (int i=0; i<26; i++) {
            if ((char) (i+97+shifter) <='z') {
                System.out.print((char) (i+97+shifter) + " ");
            }
            else {
                System.out.print((char) (i+71+shifter) + " ");
            }
        }
        System.out.println("");
        sline = encrypt(sline, shifter);
        System.out.println(sline);
        sline = decrypt(sline, shifter);
        System.out.println(sline);

    }

    public static String encrypt(String x, int y)
    {
        // 65-90 + 97-122
        StringBuilder aux = new StringBuilder(x);
        for (int i=0; i<x.length(); i++){
            if (aux.charAt(i) != ' ')
            aux.setCharAt(i, (char) ((int) x.charAt(i) + y));
            if ((aux.charAt(i) > 'z' || (aux.charAt(i) > 'Z' && aux.charAt(i) < 'a')) && aux.charAt(i) != ' ') {
                aux.setCharAt(i, (char) ((int) aux.charAt(i) - 26));
            }
        }
        return aux.toString();
    }

    public static String decrypt(String x, int y)
    {
        // 65-90 + 97-122
        StringBuilder aux = new StringBuilder(x);
        for (int i=0; i<x.length(); i++){
            if (aux.charAt(i) != ' ')
                aux.setCharAt(i, (char) ((int) x.charAt(i) - y));
            if ((aux.charAt(i) < 'A' || (aux.charAt(i) < 'a' && aux.charAt(i) > 'Z')) && aux.charAt(i) != ' ') {
                aux.setCharAt(i, (char) ((int) aux.charAt(i) + 26));
            }
        }
        return aux.toString();
    }
}
