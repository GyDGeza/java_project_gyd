package packet_exemplu;

import java.util.Scanner;

public class Exercitii1Doi {
    public static void main(String[] args) {
        System.out.println("Introduce inch");
        Scanner sc = new Scanner(System.in);
        double inch = sc.nextDouble();

        System.out.println("echivalent in cm: " + inchToCm(inch));
    }

    public static double inchToCm(double x)
    {
        return x * 2.54;
    }
}
