package SetExercitii1;

import java.util.Scanner;

public class Ex5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Introduce lungime array: ");
        int l = sc.nextInt();
        int[] ar = new int[l];
        for (int i=0; i<l; i++)
        {
            System.out.print("array[" + i + "] = ");
            ar[i] = sc.nextInt();
        }
        System.out.println("Media elementelor arrayului este " + avg(ar));
    }

    public static double avg(int[] x)
    {
        double avg = 0;
        for (int i = 0; i<x.length; i++)
        {
            avg += x[i];
        }
        return avg/x.length;
    }
}
