package SetExercitii1;

import java.util.Scanner;

public class Ex6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("introduce lungime array: ");
        int l = sc.nextInt();
        int[] ar = new int[l];
        for (int i=0; i<l; i++)
        {
            System.out.print("array[" + i + "] = ");
            ar[i] = sc.nextInt();
        }
        System.out.println("In array se afla " + countEven(ar) + " numere pare si " + countOdd(ar) + " numere impare.");
    }

    public static int countOdd(int[] x)
    {
        int counter = 0;
        for (int i=0; i<x.length; i++)
        {
            if (x[i] % 2 != 0)
            {
                counter++;
            }
        }
        return counter;
    }

    public static int countEven(int[] x)
    {
        int counter = 0;
        for (int i=0; i<x.length; i++)
        {
            if (x[i] % 2 == 0)
            {
                counter++;
            }
        }
        return counter;
    }
}
