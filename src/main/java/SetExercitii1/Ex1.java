package SetExercitii1;

import java.util.Scanner;

public class Ex1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Citeste Fahrenheit: ");
        double F = sc.nextDouble();
        System.out.println("Echivalent in Celsius: " + convertFtoC(F));
    }

    public static double convertFtoC(double x)
    {
        return (x - 32) / 1.8;
    }
}
