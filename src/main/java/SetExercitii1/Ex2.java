package SetExercitii1;

import java.util.Scanner;

public class Ex2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Numar de minute: ");
        int nr = sc.nextInt();
        System.out.println("Echivalent a " + countYears(nr) + " ani si " + (countDays(nr) - countYears(nr) * 365) + " zile.");
    }

    public static int countDays(int x)
    {
        return x/1440;
    }

    public static int countYears(int x)
    {
        return countDays(x)/365;
    }
}
