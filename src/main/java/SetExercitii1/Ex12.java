package SetExercitii1;

import java.util.Scanner;

public class Ex12 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Citeste stringul: ");
        String sline = sc.nextLine();
        System.out.println("Stringul cerut este " + evenify(sline));
    }

    public static String evenify(String x)
    {
        String newX = "";
        for (int i = 1; i<x.length(); i += 2)
        {
            newX += x.charAt(i);
        }
        return newX;
    }
}
