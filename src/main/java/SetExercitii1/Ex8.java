package SetExercitii1;

import java.util.Scanner;

public class Ex8 {
    public static void main(String[] args) {
        int[] ar = arrayRead();
        int avg = 0, cnt = 0;
        for (int i=0; i<ar.length; i++)
        {
            if (ar[i] != arrayMax(ar) || ar[i] != arrayMin(ar))
            {
                avg += ar[i];
                cnt++;
            }
        }
        System.out.println("Media elementelor din array fara cele mai mici si mari este " + (avg/cnt));
    }

    public static int[] arrayRead()
    {
        Scanner sc = new Scanner(System.in);
        System.out.print("Introduce lungime array: ");
        int l = sc.nextInt();
        int[] ar = new int[l];
        for (int i=0; i<l; i++)
        {
            System.out.print("array[" + i + "] = ");
            ar[i] = sc.nextInt();
        }
        return ar;
    }

    public static int arrayMin(int[] ar)
    {
        int min = ar[0];
        for (int i=1; i<ar.length; i++)
        {
            if (ar[i] < min)
            {
                min = ar[i];
            }
        }
        return min;
    }

    public static int arrayMax(int[] ar)
    {
        int max = ar[0];
        for (int i=1; i<ar.length; i++)
        {
            if (ar[i] > max)
            {
                max = ar[i];
            }
        }
        return max;
    }
}
