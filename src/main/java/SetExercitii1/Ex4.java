package SetExercitii1;

import java.util.Scanner;

public class Ex4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("introduce numar: ");
        int nr = sc.nextInt();
        System.out.println("primele " + nr + " numere naturale sunt: ");
        for (int i=0; i<nr-1; i++)
        {
            System.out.print((i+1) + ", ");
        }
        System.out.println(nr);
        System.out.println("suma primelor " + nr + " numere naturale este " + sum(nr));
    }

    public static int sum(int x)
    {
        int sum = 0;
        for (int i=0; i<=x; i++)
        {
            sum += i;
        }
        return sum;
    }

}
