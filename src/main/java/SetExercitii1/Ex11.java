package SetExercitii1;

import java.util.Scanner;

public class Ex11 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Citeste stringul: ");
        String sline = sc.nextLine();
        System.out.print("Stringul " + sline );
        if (numberCheck(sline))
        {
            System.out.println(" contine numai numere");
        }
        else
        {
            System.out.println(" contine caractere nenumerice");
        }
    }

    public static boolean numberCheck(String x)
    {
        boolean check = true;
        for (int i=1; i<x.length(); i++)
        {
            if (x.charAt(i) < '0' || x.charAt(i) > '9')
            {
                check = false;
                break;
            }
        }
        return check;
    }
}
