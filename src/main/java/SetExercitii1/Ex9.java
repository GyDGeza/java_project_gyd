package SetExercitii1;

import java.util.Scanner;

public class Ex9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Citeste stringul: ");
        String sline = sc.nextLine();
        System.out.print("Citeste caracterul de inlocuit: ");
        char char1 = sc.nextLine().charAt(0);
        System.out.print("Citeste caracterul inlocuitor: ");
        char char2 = sc.nextLine().charAt(0);
        sline = sline.replace(char1,char2);
        System.out.println(sline);
    }


}
