package SetExercitii1;

import java.util.Scanner;

public class Ex14 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Introduce figura: ");
        String fig = sc.nextLine();
        if (fig.equals("patrat"))
        {
            System.out.print("Introduce lungime latura: ");
            int latura = sc.nextInt();
            System.out.println("Perimetrul este " + (latura*4) + " si aria este " + (latura*latura));
        }
        else if (fig.equals("dreptunghi"))
        {
            System.out.print("Introduce lungime: ");
            int laturaL = sc.nextInt();
            System.out.print("Introduce latime: ");
            int latural = sc.nextInt();
            System.out.println("Perimetrul este " + (laturaL*2 + latural*2) + " si aria este " + (laturaL*latural));
        }
        else if (fig.equals("triunghi"))
        {
            System.out.print("Introduce latura1: ");
            int l1 = sc.nextInt();
            System.out.print("Introduce latura2: ");
            int l2 = sc.nextInt();
            System.out.print("Introduce latura3: ");
            int l3 = sc.nextInt();
            System.out.print("Introduce inaltime: ");
            int inaltime = sc.nextInt();
            System.out.println("Perimetrul este " + (l1 + l2 + l3) + " si aria este " + (l1*inaltime/2));
        }
        else if (fig.equals("cerc"))
        {
            System.out.print("Introduce raza: ");
            int raza = sc.nextInt();
            System.out.println("Perimetrul este " + (2 * 3.14 * raza) + " si aria este " + (3.14 * (raza*raza)));
        }
        else
        {
            System.out.println("string introdus gresit");
        }
    }
}
